use core::{
    future::Future,
    pin::Pin,
    task::{
        Context,
        Poll,
        Waker,
    },
};

use rtic::Mutex;

pub struct WakeFlag {
    waker: Option<Waker>,
    woken: bool,
}

impl WakeFlag {
    pub const fn new() -> WakeFlag {
        WakeFlag {
            waker: None,
            woken: false,
        }
    }
    pub fn wake(&mut self) {
        if let Some(waker) = self.waker.take() {
            waker.wake();
        }
        self.woken = true;
    }
}

pub trait WakeFlagExt {
    fn wait(&mut self) -> WaitFut<Self>;
}

impl<M> WakeFlagExt for M
where
    M: Mutex<T = WakeFlag>,
{
    fn wait(&mut self) -> WaitFut<Self> {
        WaitFut { flag: self }
    }
}

pub struct WaitFut<'a, M: ?Sized> {
    flag: &'a mut M,
}

impl<'a, M> Future for WaitFut<'a, M>
where
    M: Mutex<T = WakeFlag>,
{
    type Output = ();
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<()> {
        let this = unsafe { self.get_unchecked_mut() };
        this.flag.lock(|flag| {
            let woken = flag.woken;
            if woken {
                flag.woken = false;
                Poll::Ready(())
            } else {
                let new_waker = cx.waker();
                if let Some(ref mut waker) = flag.waker {
                    if !new_waker.will_wake(waker) {
                        *waker = new_waker.clone();
                    }
                } else {
                    flag.waker = Some(new_waker.clone());
                }
                Poll::Pending
            }
        })
    }
}
