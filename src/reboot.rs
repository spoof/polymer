pub fn reboot_bootloader(_: crate::hal::backup_domain::BackupDomain) -> ! {
    unsafe {
        // Note: Using raw offsets here because it's wrong in the upstream stm32f1 crate.
        const BKP_BASE: usize = 0x4000_6C00;
        const BKP_DR10_OFF: usize = 0x28;
        core::ptr::write_volatile((BKP_BASE + BKP_DR10_OFF) as *mut _, 0x424C)
    };
    reboot();
}

pub fn reboot() -> ! {
    crate::stm32::SCB::sys_reset()
}
