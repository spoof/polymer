macro_rules! set_allocator {
    () => {
        use alloc::alloc::Layout;

        use alloc_cortex_m::CortexMHeap;

        const HEAP_SIZE: usize = 16 * 1024;

        #[global_allocator]
        static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

        #[alloc_error_handler]
        pub fn rust_oom(_: Layout) -> ! {
            loop {}
        }

        fn init_allocator() {
            let start = rt::heap_start() as usize;
            unsafe { ALLOCATOR.init(start, HEAP_SIZE) }
        }
    };
}
